import React from "react";


function Item ({value, className}){
    return (
        <div className={className}>{value}</div>
    )
}


export default Item;