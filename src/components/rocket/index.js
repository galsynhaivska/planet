import React from "react";

import "./rocket.css";

function Rocket({className, src, alt}){
    return (
        <div className={className}>
            <img src={src} alt={alt}></img>
        </div>
    )
}

export default Rocket