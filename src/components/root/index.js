import {Component} from "react";
import Item from "../item";
import Earth from "../earth";
import Rocket from "../rocket";
import Text from "../text";


import earthSmall from "../../image/earth-small.png";
import btnImage from "../../image/btn-image.svg";
import imgRocket from "../../image/rocket.png";

import "./root.css";

const maxSize = 2.5,
    minSize = 2.3;
const starWidth = window.innerWidth;
const starHeight = window.innerHeight;
console.log( starWidth, starHeight);
let stars = [];

for(let i = 0; i < 200; i++){
    let  math1 = Math.random();
    let  math2 = Math.random();
    let  math3 = Math.random();
    let  math4 = Math.random();
//    let  math5 = Math.random();
    //let newStar = new StarItem(`${((maxSize - minSize) * math1 + minSize).toFixed(2)}`, `${((maxSize - minSize) * math2 + minSize).toFixed(2)}`,`${(starHeight * math3).toFixed(2)}`,`${(starWidth * math4).toFixed(2)}` , `${(180 * math5 - 90).toFixed(2)}` );
    let newStar = {
        width: `${((maxSize - minSize) * math1 + minSize).toFixed(2)}px`,
        height: `${((maxSize - minSize) * math2 + minSize).toFixed(2)}px`,
        top: `${(starHeight * math3).toFixed(2)}px`,
        left: `${(starWidth * math4).toFixed(2)}px` ,
    }
    stars.push(newStar);
}
//console.log( stars);


class Root extends Component{
    state = {
        image: "",
        top: "",
        item0: "",
        item1: "",
        item2: "",
        item3: "",
        item4: "",
        title: "",
        rocket: "",
        info: "",
        currentMinutes : new Date().getMinutes(),     
    }

    click = () => {
        this.setState((state) =>{
            return{
                ...state,
                image: "img-after-click",  
            }
        })
        const nav = document.querySelectorAll(".nav-item")
        for(let i = 0; i < nav.length; i++){
            setTimeout(()=>{
                nav[i].style.top = "70px";
                if(i === 0){
                    this.setState((state) =>{
                        return{
                            ...state,                       
                        item0: "item-click", 
                        }
                    }) 
                }else if(i === 1){
                    this.setState((state) =>{
                        return{
                            ...state,                       
                        item1: "item-click", 
                        }
                    }) 
                }else if(i === 2){
                    this.setState((state) =>{
                        return{
                            ...state,                       
                        item2: "item-click", 
                        }
                    }) 
                }else if(i === 3){
                    this.setState((state) =>{
                        return{
                            ...state,                       
                        item3: "item-click", 
                        }
                    }) 
                }else if(i === 4){
                    this.setState((state) =>{
                        return{
                            ...state,                       
                        item4: "item-click", 
                        }
                    }) 
                }                        
            }, 1000 + i*500);
        }
        setTimeout(()=>{
        this.setState((state) =>{
            return{
                ...state,
                title: "show-title", 
            }     
        })
    }, 4000)
    
    setTimeout(()=>{
        document.querySelector(".btn-bottom").style.display = "block";
        this.setState((state) =>{
            return{
                ...state,
            }     
        })
    }, 7000);
    }

    showInfo = () => {
        document.querySelector(".btn-bottom").style.display = "none";
        this.setState((state) =>{
            return{
                ...state,
                title: "",
                top: " earth-top",
                rocket: "show-rocket",  
            }     
        })  
        setTimeout(()=>{
            this.setState((state) =>{
                return{
                    ...state,
                    info: "show-info",
                }     
            })
        }, 2000);
        
    }
     
    render() {
        return (
            <>
                <div className="nav-container">
                    <Item className={"nav-item item1 " + this.state.item0} value="Inicio"></Item>
                    <Item className={"nav-item item2 " + this.state.item1} value="Espacio"></Item>
                    <Item className={"nav-item item3 " + this.state.item2} value="Planetas"></Item>
                    <Item className={"nav-item item4 " + this.state.item3} value="Nosotros"></Item>
                    <Item className={"nav-item item5 " + this.state.item4} value="Misiones"></Item>
                </div>
                
                <h1 className={"h1-title " + this.state.title}>Un viaje al espacio</h1>
                <div className="main-container">
                {stars.map((el, i) => {
                    return  <div className="star" style={{width: el.width, height: el.height, top: el.top, left: el.left}}  key={"star" + i}></div>
                 })}         
                    <Earth className={"img-container " + this.state.image + this.state.top} className1="earth"  click={this.click} src={earthSmall} value="The Earth"></Earth>
                </div>
                <Rocket className={"div-rocket " + this.state.rocket} src={imgRocket} alt="Rocket"></Rocket>
                <Text className={"rocket-section " + this.state.info} classTitle="title-section" value="Titulo" classText="text-section" info="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Semper viverra tempor, enim vulputate nunc interdum sit diam trices. Sed erat volutpat curabitur ornare in facilisiornare. Vitae mollis sed feugiat ipsum condimentum eget magnis nulla at. Massa semper massa quisque tincidunt cursus. Elementum aliquet sed lectus facilisis massa in. Felis lectus egestas urna egestas arcu. Quam quisque volutpat lacus, eget. Quis risus, rhoncus nisi a, sit libero ut viverra. Magna quis hendrerit in cursus. Sed sed vitae ullamcorper dignissim tristique. Imperdiet vulputate landit eu egestas massa a mauris libero. Mi turpis sagittis ac elit id sollicitudin urna. Velit neque
                neque vitae ultrices sagittis hendrerit in cursus. Sed egestas commodo mi sed. Aliquam at nunc, vestibulum viverra ipsum. Libero scelerisque tortor pellentesque ante ut sit nunc, vitae. ulla donec ultrices quis eu adipiscing habitant.">
                </Text> 
                <Earth className="btn-bottom" className1=""  click={this.showInfo}  src={btnImage} value="button"></Earth>
            </>
        )
    }
}
export default Root;