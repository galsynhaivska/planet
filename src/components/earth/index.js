import React from "react";
import "./earth.css"


function Earth ({className, className1, src, value, click}) {
    return (
        <div className={className}>
            <img className={className1} onClick={()=>{click()}} src={src} alt={value}></img>
        </div>
    )
}
export default Earth;