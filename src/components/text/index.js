import React from "react";

import "./text.css";

function Text({className, classTitle, classText, value, info}){
    return (
        <section className={className}>
            <h2 className={classTitle}>{value}</h2>
            <p className={classText}> {info}</p>
        </section>
    )
}
export default Text;